.. This README is meant for consumption by humans and pypi. Pypi can render rst files so please do not use Sphinx features.
   If you want to learn more about writing documentation, please check out: http://docs.plone.org/about/documentation_styleguide.html
   This text does not appear on pypi or github. It is a comment.

=============
uniud.planner
=============

Manage resource booking using standard plone events as timeslots and standard plone folders as resources.

Features
--------

- Users can manages timeslot bookings for defined resources
  - anonymous users can see free timeslots in the timetable
  - authenticated users can book a free timeslot or delete own future booking

  booking events are normal plone events: no particoular role is needed to book a timeslot (*Contributor* role is proxied by *Planner* view to manage CRUD operations)

- Editors defines resources available for booking

  Editors adds a normal plone folder as *resource category folder* (i.e. "Rooms") and apply "Planner" view, then, inside the category folder adds folders as *resource folders* ("i.e. "Room 1", "Room 2", ...). Planner view will display them as *lines* of a booking timetable, where timeslots available are the *columns* of the timetable.

- Managers define timeslots available as a "per site" configuration, for example:
  ::

    9
    10
    11
    16
    17

  ...defines timeslots: 9:00-9:59, 10:00-10:59, 11:00-11:59, 16:00-16:59, 17:00-17:59

- A portlet: "My bookings" is available to show next bookings for current logged user

Examples
--------

This add-on can be seen in action at the following sites:
- Is there a page on the internet where everybody can see the features?


Documentation
-------------

Will do, I promise


Translations
------------

This product has been translated into

- Klingon (thanks, K'Plai)


Installation
------------

Install uniud.planner by adding it to your buildout::

    [buildout]

    ...

    eggs =
        uniud.planner


and then running ``bin/buildout``


Contribute
----------

- Issue Tracker: https://github.com/collective/uniud.planner/issues
- Source Code: https://github.com/collective/uniud.planner
- Documentation: https://docs.plone.org/foo/bar


Support
-------

If you are having issues, please let us know.
We have a mailing list located at: project@example.com


License
-------

The project is licensed under the GPLv2.
