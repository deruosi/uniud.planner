# -*- coding: utf-8 -*-
from plone.app.testing import setRoles, TEST_USER_ID
from plone.portlets.interfaces import IPortletType
from uniud.planner.testing import (
    UNIUD_PLANNER_FUNCTIONAL_TESTING,
    UNIUD_PLANNER_INTEGRATION_TESTING,
)
from zope.component import getUtility

import unittest


class PortletIntegrationTest(unittest.TestCase):

    layer = UNIUD_PLANNER_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.app = self.layer['app']
        self.request = self.app.REQUEST
        setRoles(self.portal, TEST_USER_ID, ['Manager'])

    def test_mybookings_is_registered(self):
        portlet = getUtility(
            IPortletType,
            name='uniud.planner.portlets.Mybookings',
        )
        self.assertEqual(portlet.addview, 'uniud.planner.portlets.Mybookings')


class PortletFunctionalTest(unittest.TestCase):

    layer = UNIUD_PLANNER_FUNCTIONAL_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
