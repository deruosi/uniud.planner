# -*- coding: utf-8 -*-
from __future__ import absolute_import
from Acquisition import aq_inner
from plone import schema, api
from plone.app.portlets.portlets import base
from plone.memoize.instance import memoize
from plone.portlets.interfaces import IPortletDataProvider
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from uniud.planner import _
from z3c.form import field
from zope.component import getMultiAdapter
from zope.interface import implementer
import pytz
from plone.app.event.base import default_timezone
from datetime import datetime


class IMybookingsPortlet(IPortletDataProvider):
    count = schema.Int(title=_(u'Number of items to display'),
                       description=_(u'How many items to list.'),
                       required=True,
                       default=5)


@implementer(IMybookingsPortlet)
class Assignment(base.Assignment):
    schema = IMybookingsPortlet

    def __init__(self, count=5):
        self.count = count

    @property
    def title(self):
        return _(u'My bookings')


class AddForm(base.AddForm):
    schema = IMybookingsPortlet
    form_fields = field.Fields(IMybookingsPortlet)
    label = _(u'Add My bookings')
    description = _(u'This portlet displays my bookings.')

    def create(self, data):
        return Assignment(
            count=data.get('count', 5),
        )


class EditForm(base.EditForm):
    schema = IMybookingsPortlet
    form_fields = field.Fields(IMybookingsPortlet)
    label = _(u'Edit My bookings')
    description = _(u'This portlet displays my bookings.')


class Renderer(base.Renderer):
    schema = IMybookingsPortlet
    _template = ViewPageTemplateFile('mybookings.pt')

    def __init__(self, *args):
        base.Renderer.__init__(self, *args)
        context = aq_inner(self.context)
        portal_state = getMultiAdapter(
            (context, self.request),
            name=u'plone_portal_state'
        )
        self.anonymous = portal_state.anonymous()

    def render(self):
        return self._template()

    @property
    def available(self):
        """Show the portlet only if there are one or more elements and
        not an anonymous user."""
        return not self.anonymous and self._data()

    def my_bookings(self):
        self.result = self._data()
        return self.result

    @memoize
    def _data(self):
        limit = self.data.count
        local_tz = pytz.timezone(default_timezone()) #should be taken from portal tz
        now = datetime.now(local_tz)
        current_userid = api.user.get_current().getId()
        result = api.content.find(portal_type='Event', Creator=current_userid)
        result = [brain for brain in result if brain.start > now]
        return result[:limit]