# -*- coding: utf-8 -*-
from plone import api
from plone.app.vocabularies.terms import safe_simplevocabulary_from_values
from zope.interface import implementer
from zope.schema.interfaces import IVocabularyFactory


@implementer(IVocabularyFactory)
def TimeslotsVocabularyFactory(context):
    values = api.portal.get_registry_record('uniud.planner.available_timeslots')
    return safe_simplevocabulary_from_values(values)
