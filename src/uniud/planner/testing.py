# -*- coding: utf-8 -*-
from plone.app.contenttypes.testing import PLONE_APP_CONTENTTYPES_FIXTURE
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import applyProfile
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting
from plone.app.testing import PloneSandboxLayer
from plone.testing import z2

import uniud.planner


class UniudPlannerLayer(PloneSandboxLayer):

    defaultBases = (PLONE_APP_CONTENTTYPES_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load any other ZCML that is required for your tests.
        # The z3c.autoinclude feature is disabled in the Plone fixture base
        # layer.
        import plone.restapi

        self.loadZCML(package=plone.restapi)
        self.loadZCML(package=uniud.planner)

    def setUpPloneSite(self, portal):
        applyProfile(portal, "uniud.planner:default")


UNIUD_PLANNER_FIXTURE = UniudPlannerLayer()


UNIUD_PLANNER_INTEGRATION_TESTING = IntegrationTesting(
    bases=(UNIUD_PLANNER_FIXTURE,),
    name="UniudPlannerLayer:IntegrationTesting",
)


UNIUD_PLANNER_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(UNIUD_PLANNER_FIXTURE,),
    name="UniudPlannerLayer:FunctionalTesting",
)


UNIUD_PLANNER_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        UNIUD_PLANNER_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE,
    ),
    name="UniudPlannerLayer:AcceptanceTesting",
)
