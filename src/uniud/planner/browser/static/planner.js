function setCookie(name,value,sec) {
    var expires = '';
    if (sec) {
        var date = new Date();
        date.setTime(date.getTime() + sec*1000);
        expires = '; expires=' + date.toUTCString();
    }
    document.cookie = name + '=' + (value || '')  + expires + '; path=/';
}
function getCookie(name) {
    var nameEQ = name + '=';
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

document.addEventListener('DOMContentLoaded', function(event) {
    // get the container of the table to be scrolled
    var planner = document.querySelector('.planner-scroll');
    // get the scroll in the cookie and eventually scroll the table
    var scroll = getCookie('planner-scroll')
    if (scroll) {
        planner.scrollLeft=scrollLeft = scroll.split(':')[0];
        planner.scrollTop=scrollTop = scroll.split(':')[1];
    }
    planner.addEventListener('click', function() {
        var scroll=planner.scrollLeft+':'+planner.scrollTop;
        console.log('clicked! (scrollLeft:scrollTop) '+scroll);
        // set the cookie time to 10 sec: it should be enough to permit page reload
        setCookie('planner-scroll',scroll,10);
    }, false);  });