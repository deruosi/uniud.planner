from calendar import Calendar
from datetime import date, datetime, time, timedelta
from plone import api
from plone.app.event.base import default_timezone
from plone.memoize.view import memoize
from plone.protect.authenticator import createToken
from Products.Five.browser import BrowserView

import logging
import pytz


logger = logging.getLogger('Plone')

class PlannerView(BrowserView):
    """
    Manages timeslot booking for the resources
    """
    def __init__(self, context, request):
        # Each view instance receives context and request as construction parameters
        self.context = context
        self.request = request
        self.local_tz = pytz.timezone(default_timezone()) #should be taken from portal tz
        self.now = datetime.now(self.local_tz)
#        logger.info('default_timezone: ' + default_timezone())

    def is_planner_view(self):
        """Checks if default view for context is: planner"""
        context = self.context
        layout = context.getProperty('layout', None)
        logger.info('layout: {}'.format(layout))
        return (layout == 'planner')

    def get_planner(self):
        """Implements CRUD operations for booking events in the context folder"""        
        start_profiling_dt = datetime.now()
        # logger.info('{}ms start get_planner'.format(start_profiling_dt.microsecond//1000))
        query_string = self.request.form
        # logger.info('query_string: ' + str(query_string))
        day_str = ''
        if query_string:
            if 'day' in query_string:
                day_str = query_string['day']
            if all (key in query_string for key in ('resource','timeslot')):
                resource = query_string['resource']
                timeslot = int(query_string['timeslot'])
                self.add_booking_event(resource, timeslot, day_str)
            if 'uid' in query_string:
                uid = query_string['uid']
                self.del_booking_event(uid)
        planner = self.get_bookings(day_str)
        profiling_td = datetime.now() - start_profiling_dt
        logger.info('profiling: get_planner executed in {}.{:03d} sec'.format(profiling_td.seconds, profiling_td.microseconds//1000))
        return planner

    def get_bookings(self, day_str):
        """Populates planner list"""
        day_d = self.get_day(day_str)
        context = self.context
        planner = []
        my_userid = api.user.get_current().getId()
        available_timeslots = [int(timeslot_str) for timeslot_str in api.portal.get_registry_record('uniud.planner.available_timeslots')]
        for resource in context.restrictedTraverse('@@contentlisting')(portal_type='Folder'):
            resource_events = [event for event in api.content.find(path=resource.getPath(), depth=1, portal_type='Event') if event.start.date() == day_d]
            # logger.info('{}ms {} events found in resource: {}'.format(datetime.now().microsecond//1000, len(resource_events), resource.id))
            bookings = []
            for timeslot in available_timeslots:
                is_future = self.is_future(timeslot_str=timeslot, day_str=day_str)
                is_free = True
                booked_by_me = False
                uid=''
                for booking_event in resource_events:
                    start_h = booking_event.start.hour
                    if start_h == timeslot:
                        is_free = False
                        # logger.info('booking Event found in day: {} timeslot: {}'.format(day_d, timeslot))
                        if booking_event.Creator == my_userid:
                            booked_by_me = True
                            uid = booking_event.UID
                        # if I find a booking Event in a timeslot, it should be unique: no need to process others
                        break
                flags = ''
                if is_free:
                    flags += ' free'
                else:
                    flags += ' booked'
                if booked_by_me:
                    flags += ' booked-by-me'
                if is_future:
                    flags += ' is-future'
                else:
                    flags += ' is-past'
                # # logger.info('{}ms flags: {}'.format(datetime.now().microsecond//1000, flags))
                bookings.append({
                    'timeslot':timeslot,
                    'is_free':is_free,
                    'booked_by_me':booked_by_me,
                    'is_future':is_future,
                    'flags':flags,
                    'uid':uid
                    })
            resource_bookings = {'title':resource.title, 'id':resource.id, 'bookings':bookings}
            # logger.info('{}ms resource_bookings: {}'.format(datetime.now().microsecond//1000, resource_bookings))
            planner.append(resource_bookings)
        # logger.info('{}ms end get_planner'.format(datetime.now().microsecond//1000))
        return planner

    def get_token(self):
        """CSRF protection from plone.protect"""
        return createToken()

    def get_timeslots(self):
        """Available timeslot configured in the registry"""
        available_timeslots = api.portal.get_registry_record('uniud.planner.available_timeslots')
        # logger.info('{}ms read from registry: {}'.format(datetime.now().microsecond//1000, available_timeslots))
        return available_timeslots

    def get_day(self, day_str='', localized=False):
        """Day string to day or today datetime object"""
        if day_str and day_str!='None':
            # date.today().strftime('%Y-%m-%d') = '2018-09-13'
            # datetime.strptime('2018-9-13', '%Y-%m-%d').date() = datetime.date(2018, 9, 13)
            day_d = datetime.strptime(day_str, '%Y-%m-%d').date()
        else:
            day_d = date.today()
        if localized == True:
            day_dt = datetime.combine(day_d, time())
            result = api.portal.get_localized_time(datetime=day_dt)
        else:
            result = day_d
        return result


    def get_calendar(self, day_str=''):
        """Calendar of current month"""
        start_profiling_dt = datetime.now()
        now = self.now
        current_calendar = []
        current_weeks = Calendar().monthdatescalendar(now.year, now.month)
        for week in current_weeks:
            for day in week:
                formatted = day.strftime('%Y-%m-%d')
                flags = ''
                if day == date.today():
                    flags += 'is-today '
                    if not day_str:
                        flags += 'is-selected '
                if formatted == day_str:
                    flags += 'is-selected '
                if day < date.today():
                    flags += 'is-past '
                current_calendar.append({
                    'formatted':day.strftime('%Y-%m-%d'),
                    'localized':api.portal.get_localized_time(datetime=day),
                    'number':day.strftime('%d'),
                    'flags':flags,
                    })
        profiling_td = datetime.now() - start_profiling_dt
        logger.info('profiling: get_calendar executed in {}.{:03d} sec'.format(profiling_td.seconds, profiling_td.microseconds//1000))
        return current_calendar

    @memoize
    def is_future(self, timeslot_str, day_str=''):
        """True if booking Event exists and starts in the future"""
        now = self.now
        local_tz = self.local_tz
        is_future = False
#        loginfo = []
#        loginfo.append(str(datetime.now().microsecond//1000)+'ms')
        # check booking Event identified by date and timeslot
        if day_str and day_str!='None':
#            loginfo.append(' day {} supplied,'.format(day_str))
            day_d = datetime.strptime(day_str, '%Y-%m-%d').date()
        else:
            day_d = date.today()
        if timeslot_str:
#                loginfo.append(' timeslot {} supplied,'.format(timeslot_str))
                start_t = time(int(timeslot_str))
                start_dt_naive = datetime.combine(day_d,start_t)
                start_dt = local_tz.localize(start_dt_naive)
                if start_dt > now:
                    is_future = True
#        loginfo.append(' is_future: {}'.format(is_future))
#        logger.info(''.join(loginfo))
        return is_future

    def add_booking_event(self, resource, timeslot_str, day_str):
        """Adds a new booking Event in the requested resource/timeslot"""
        if not api.user.is_anonymous():
            # logger.info('{}ms start add_booking_event'.format(datetime.now().microsecond//1000))
            local_tz = self.local_tz
            context = self.context
            day_d = self.get_day(day_str)
            now = self.now
            # logger.info('booking resource: {}, day: {} timeslot_str: {}'.format(resource, day_str, timeslot_str))
            resource_obj = context[resource]
            start_t = time(timeslot_str)
            start_dt_naive = datetime.combine(day_d,start_t)
            start_dt = local_tz.localize(start_dt_naive)
            # check resource/timeslot to avoid duplicates
            previous_bookings = api.content.find(
                context=resource_obj,
                portal_type='Event',
                start=start_dt
            )
            # check if another timeslot was created just now (during this second)
            if start_dt < now:
                logger.info('booking error: Event starting at: %s is in the past: skip creation' % start_dt)
            elif previous_bookings:
                logger.info('booking conflict: another Event found in: %s, starting at: %s: skip creation' % (resource, start_dt))
            else:
                # logger.info('creating Event in: %s starting at: %s ending at: %s' % (resource, start_dt, end_dt))
                reservation_title = resource_obj.title + ' booked'
                with api.env.adopt_roles(['Contributor']):
                    booking_obj = api.content.create(
                        type='Event',
                        title=reservation_title,
                        container=resource_obj,
                        start=start_dt,
                        end=start_dt + timedelta(minutes=59),
                        id=str(now.strftime("%s.%f")),
                    )
                # logger.info('{}ms Event: {} created'.format(datetime.now().microsecond//1000, booking_obj.id))
                sametime_reservations = api.content.find(
                    context=resource_obj,
                    start=start_dt
                )
                #avoid race condition duplicates
                if len(sametime_reservations) > 1:
                    logger.info('booking conflict (race condition): another Event found in: %s, starting at: %s: delete duplicate' % (resource, start_dt))
                    api.content.delete(obj=booking_obj)
                else:
                    with api.env.adopt_roles(['Reviewer']):
                        api.content.transition(obj=booking_obj, transition='publish')
                        logger.info('{}ms Event: {} created and published'.format(datetime.now().microsecond//1000, booking_obj.id))
        else:
            logger.info('user is anonymous: must be authenticated to book a resource')

    def del_booking_event(self, uid):
        """Deletes a booking Event by uid"""
        # logger.info('{}ms start del_booking_event'.format(datetime.now().microsecond//1000))
        booking_obj = api.content.get(UID=uid)
        now = self.now
        if not booking_obj:
            logger.info('UID: %s object not found: skip deleting' % uid)
        elif booking_obj.start < now:
            logger.info('UID: %s object start: %s is in the past: skip deleting' % (uid, booking_obj.start))
        else:
            logger.info('{}ms Event: {} deleted'.format(datetime.now().microsecond//1000, booking_obj.id))
            api.content.delete(obj=booking_obj)
        # logger.info('{}ms end del_booking_event'.format(datetime.now().microsecond//1000))
