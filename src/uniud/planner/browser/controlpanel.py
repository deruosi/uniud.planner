# -*- coding: utf-8 -*-
from plone.app.registry.browser.controlpanel import (
    ControlPanelFormWrapper,
    RegistryEditForm,
)
from plone.z3cform import layout
from zope import schema
from zope.interface import Interface


class IUniudPlannerControlPanel(Interface):

    available_timeslots = schema.Tuple(
        title=u'Available timeslots',
        description=u'Define timeslots available for booking',
        default=(u'12',u'13',u'14',u'15',u'16',u'17',u'18',u'19'),
        missing_value=None,
        required=False,
        value_type=schema.TextLine(),
    )


class UniudPlannerControlPanelForm(RegistryEditForm):
    schema = IUniudPlannerControlPanel
    schema_prefix = "uniud.planner"
    label = u'uniud.planner Settings'


UniudPlannerControlPanelView = layout.wrap_form(
    UniudPlannerControlPanelForm, ControlPanelFormWrapper)